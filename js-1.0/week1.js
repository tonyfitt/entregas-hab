/**
 * El objetivo del ejercicio es crear un nuevo array que contenga
 * todos los hashtags del array `tweets`, pero sin repetir
 * 
 * Nota: como mucho hay 2 hashtag en cada tweet
 */

tweets = [
    'aprendiendo #javascript en  Vigo',
    'empezando el segundo módulo del bootcamp!',
    'hack a boss bootcamp vigo #javascript #codinglive '];


hashtag = [];   //Array para introducir los hashtags

for (tweet of tweets) {         //Recorremos los tweets uno por uno

    inicio = tweet.indexOf("#")
    final = tweet.indexOf(" ", inicio)
    inicio2 = tweet.indexOf("#", final)
    final2 = tweet.indexOf(" ", inicio2)
    Resul1 = tweet.slice(inicio, final)
    Resul2 = tweet.slice(inicio2, final2)
    if (hashtag.includes(Resul1) == false && (Resul1 != "")) {     //Condicion para que no introduzca los vacios ni los que ya esten.
        hashtag.push(Resul1)
    }
    if (hashtag.includes(Resul2) == false && (Resul2 != "")) {
        hashtag.push(Resul2)
    }
}
console.log(hashtag);
